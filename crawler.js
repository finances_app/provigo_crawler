const fs = require('fs');
const path = require('path');
const superagent = require('superagent');
const _ = require('lodash');
const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');

puppeteer.use(stealthPlugin());

const config = JSON.parse(fs.readFileSync(path.join(__dirname, 'config.json')));
const ORDERS_DIR = path.join(__dirname, 'orders');
const ORDERS_ERROR_DIR = path.join(__dirname, 'orders_error');

class ProvigoCrawler {
	static async scrape() {
		// Some shitty anti-crawler script forces us to run with headless: false
		this._browser = await puppeteer.launch({ slowMo: 150, defaultViewport: { width: 1920, height: 1080 } });
		this._page = await this._browser.newPage();
		
		await this._page.goto('https://www.provigo.ca/account/order-history');
		await this._page.waitForSelector('#email');
		
		await this._page.focus('#email');
		await this._page.type('#email', config.provigo_username);
		await this._page.focus('#password');
		await this._page.type('#password', config.provigo_password);
		
		await this._page.click('#login > fieldset > button');
		
		await this._page.waitForNavigation();
		await this._page.waitForSelector('.account-order-history-page');
		
		const orders = await this.getOrders();
		for (let order of orders) {
			let orderDetail = await this.getOrderDetail(order);
			fs.writeFileSync(
				path.join(ORDERS_DIR, `${order.id}.json`),
				JSON.stringify(orderDetail),
				{ encoding: 'utf-8' }
			);
		}
		
		await this.uploadOrderDetails(orders);
		
		console.log('Orders fetched successfully');
		await this._browser.close();
	}
	
	static async getOrders() {
		let orders = null;
		await this._page.reload();
		return new Promise((resolve, reject) => {
			this._page.on('response', async response => {
				try {
					if (response.url() === 'https://www.provigo.ca/api/customer/past-orders' && orders === null) {
						const existingOrders = fs.readdirSync(ORDERS_DIR);
						orders = await response.json();
						
						resolve(
							orders.filter(_order => {
								return (
									// remove all orders that were already dumped
									!existingOrders.some(_existing => _existing.includes(_order.id))
									// also exclude orders within the past 2 days, cause the transactions might not be recorded yet
									|| (Date.now() - new Date(_order.placedDate).getTime()) < 1000*60*60*24*2
								);
							})
						);
					}
				} catch (err) {
					reject(err);
				}
			});
		});
	}
	
	static async getOrderDetail(order) {
		let orderDetails = null;
		return new Promise(async (resolve, reject) => {
			let timeout = setTimeout(reject, 30*1000);
			
			this._page.on('response', async response => {
				try {
					if (response.url() === `https://www.provigo.ca/api/customer/past-orders/${order.id}` && orderDetails === null) {
						clearTimeout(timeout);
						orderDetails = await response.json();
						resolve(orderDetails);
					}
				} catch (err) {
					reject(err);
				}
			});
			await this._page.goto(`https://www.provigo.ca/account/order-history-details/${order.id}`);
		});
	}
	
	static async uploadOrderDetails(orders) {
		const authRes = await superagent.post(`${config.api_host}/api/service_authenticate`)
			.send({ "api_key": config.api_key });
		/**
		 * @property {string} access_token
		 **/
		const credentials = authRes.body;
		
		// find matching transactions for my user
		// send each new order detail json as an attachment to the matching transaction
		const filter = {
			category: 'Provigo'
		};
		const res = await superagent
			.get(`${config.api_host}/api/transaction?size=${1000}&filter=${encodeURIComponent(JSON.stringify(filter))}`)
			.set('Authorization', `Bearer ${credentials.access_token}`);
		// filter out transactions that already have a json attachment
		const transactions = res.body.items.filter(_t => !_t.attachments.some(_a => _a.type === 'json'));
		
		const transactionDateMap = new Map();
		for (let transaction of transactions) {
			transactionDateMap.set(transaction.id, new Date(transaction.date));
		}
		
		const markedTransactions = new Map();
		for (const order of orders) {
			const orderDetails = JSON.parse(
				fs.readFileSync(
					path.join(ORDERS_DIR, `${order.id}.json`), 
					'utf8'
				)
			);
			const orderDate = _.get(orderDetails, 'orderDetails.booking.timeSlot.startTime', null)
			const orderTimestamp = new Date(orderDate);
			
			// Find the transaction with the closest date
			const tuple = transactions
				.map(_transaction => {
					const transactionDate = transactionDateMap.get(_transaction.id);
					return {
						id: _transaction.id,
						distance: transactionDate.getTime() - orderTimestamp.getTime()
					}
				})
				.filter(_tuple => !markedTransactions.get(_tuple.id))
				// Here is the thing with dates:
				// - Transaction dates are always at midnight
				// - Transactions are not recorded on the week-end. So a transaction made on saturday will be dated for the next monday (or the previous friday)
				// 
				// Therefore, the two max distance edge cases are:
				// - distance is negative 48h -> the order was made at 23:59 on a saturday and the transaction was recorded for the previous friday
				// - distance is positive  48h -> the order was made at 00:00 on a saturday
				.filter(_tuple => -1000*60*60*24*2 <= _tuple.distance && _tuple.distance <= 1000*60*60*24*2)
				.sort((a, b) => Math.abs(a.distance) - Math.abs(b.distance))
				.shift();
			if (!tuple) {
				// move the file aside to that a human can take a look at it
				fs.copyFileSync(
					path.join(ORDERS_DIR, `${order.id}.json`),
					path.join(ORDERS_ERROR_DIR, `${order.id}.json`)
				);
				continue;
			}
			markedTransactions.set(tuple.id, true);
			const transactionId = tuple.id;
			
			// Upload the attachment
			const uploadRes = await superagent
				.post(`${config.api_host}/api/transaction/${transactionId}/attachment`)
				.send(orderDetails)
				.set('Authorization', `Bearer ${credentials.access_token}`);
			console.log(uploadRes.body);
		}
	}
}

ProvigoCrawler.scrape()
	.then(() => process.exit(0))
	.catch(err => {
		console.error(err);
		process.exit(1);
	});